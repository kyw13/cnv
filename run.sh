#!/bin/sh

#  run.sh
#  
#
#  Created by Kylia Williams on 3/24/19.
#  

args=./arguments.sh
source $args

mkdir $PROJECT_DIR
mkdir $PROJECT_DIR/gatk
mkdir $PROJECT_DIR/xhmm
mkdir $PROJECT_DIR/annotate
mkdir $PROJECT_DIR/output
mkdir $PROJECT_DIR/logs

# Run GATK pipeline
./gatk/00_CollectReadCounts.sh $args > $PROJECT_DIR/logs/gatk.00_CollectReadCounts.log
./gatk/01_PreprocessCNV.sh $args > $PROJECT_DIR/logs/gatk.01_PreprocessCNV.log
./gatk/02_CallCNV.sh $args > $PROJECT_DIR/logs/gatk_02.CallCNV.log
./gatk/03_PostprocessCNV.sh $PROJECT_DIR/logs/$args > gatk.03_PostprocessCNV.log
./gatk/04_MergeCalls.sh $args > $PROJECT_DIR/logs/gatk.04_MergeCalls.log

# Run XHMM pipeline
./xhmm/00_GetDepthOfCoverage.sh $args > $PROJECT_DIR/logs/xhmm.00_GetDepthOfCoverage.log
# ./xhmm/01/OptionalTargetFilter.sh $args > $PROJECT_DIR/logs/xhmm.01_OptionalTargetFilter.log
./xhmm/02_NormalizeAndCall.sh $args > $PROJECT_DIR/logs/xhmm.02_NormalizeAndCall.log
./xhmm/03_Genotype.sh $args > $PROJECT_DIR/logs/xhmm.03_Genotype.log

# Merge and annotate
./annotate/Annotate.py $PROJECT_DIR $args $annotate_concordant > $PROJECT_DIR/logs/Annotate.log
