#!/bin/sh

#  annotate_concordant.sh
#  
#
#  Created by Kylia Williams on 3/24/19.
#

source $1

gatk_input=$2
xhmm_input=$3

# Annotate concordant intervals with mappability, segmental duplications, and small repeats

$bedtools annotate -names MAPPABILITY,SEGDUP,REPEAT -i $PROJECT_DIR/annotate/concordant_gatk.bed -files mappability segdup repeats > $PROJECT_DIR/annotate/concordant_gatk_annot.bed

$bedtools annotate -names MAPPABILITY,SEGDUP,REPEAT -i $PROJECT_DIR/annotate/concordant_xhmm.bed -files mappability segdup repeats > $PROJECT_DIR/annotate/concordant_xhmm_annot.bed

# Annotate concordant intervals with AnnotSV
$annotSV -SVinputFile $PROJECT_DIR/annotate/concordant_gatk_annot.bed -SVinputInfo 1 -outputFile $PROJECT_DIR/annotate/concordant_gatk_annot2 > $PROJECT_DIR/annotate/concordant_gatk.log

$annotSV -SVinputFile concordant_xhmm_annot.bed -SVinputInfo 1 -outputFile $PROJECT_DIR/annotate/concordant_xhmm_annot2 > $PROJECT_DIR/annotate/concordant_xhmm.log
