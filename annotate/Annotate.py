#!/usr/bin/env python

import pandas as pd
from collections import Counter
import pybedtools
from pybedtools import BedTool
import subprocess
import sys

project_dir = sys.argv[1]
arguments_file = sys.argv[2]
annotate_concordant = sys.argv[3]

# Import GATK data
gatk = pd.read_table(project_dir+'/output/CNV_discover.txt')
gatk['START'] = gatk.INTERVAL.str.split(':').str[1].str.split('-').str[0]
gatk['STOP'] = gatk.INTERVAL.str.split(':').str[1].str.split('-').str[1]

# Import XHMM data
xhmm = pd.read_table(project_dir+'/output/CNV_discover.xcnv')
xhmm['START'] = xhmm.INTERVAL.str.split(':').str[1].str.split('-').str[0]
xhmm['STOP'] = xhmm.INTERVAL.str.split(':').str[1].str.split('-').str[1]

# Get concordant CNVs per sample
concordance = []
for i in set(xhmm.SAMPLE):
    a = gatk[gatk.SAMPLE==i][['CHR', 'START', 'STOP', 'SAMPLE', 'CNV', 'Q_EXACT', 'CN']].to_string(index=False, header=False)
    b = xhmm[xhmm.SAMPLE==i][['CHR', 'START', 'STOP', 'SAMPLE', 'CNV', 'Q_EXACT']].to_string(index=False, header=False)
    aBedTool = BedTool(a, from_string=True)
    bBedTool = BedTool(b, from_string=True)
    combined = aBedTool.intersect(bBedTool, wo=True, e=True) # f=0.7, F=0.7
    concordance.append(combined)

combined_cnvs = []
for i in concordance:
    combined_cnvs.append(i.to_dataframe(names=['CHR_gatk', 'START_gatk', 'END_gatk', 'SAMPLE_gatk', 'CNV_gatk', 'Q_EXACT_gatk', 'CHR_xhmm', 'START_xhmm', 'END_xhmm', 'SAMPLE_xhmm', 'CNV_xhmm', 'Q_EXACT_xhmm', 'Overlap']))
combined_cnvs = pd.concat(combined_cnvs)
combined_cnvs['length_xhmm'] = combined_cnvs.END_xhmm - combined_cnvs.START_xhmm
combined_cnvs['length_gatk'] = combined_cnvs.END_gatk - combined_cnvs.START_gatk

# Export CNVs to file for annotation
concordant_gatk = combined_cnvs[['CHR_gatk', 'START_gatk', 'END_gatk']]
concordant_gatk.to_csv(project_dir+'/annotate/concordant_gatk.bed', sep='\t', header=None, index=None)
concordant_xhmm = combined_cnvs[['CHR_xhmm', 'START_xhmm', 'END_xhmm']]
concordant_xhmm.to_csv(project_dir+'/annotate/concordant_xhmm.bed', sep='\t', header=None, index=None)

# Annotate CNVs with bedtools and AnnotSV

subprocess.call([annotate_concordant, arguments_file, 'concordant_gatk.bed', 'concordant_xhmm.bed'])

# Import GATK annotations
concordant_gatk_annot = pd.read_table(project_dir+'/annotate/concordant_gatk_annot2.tsv')
concordant_gatk_annot.rename(columns={'SV chrom': 'CHR_gatk', 'SV start': 'START_gatk', 'SV end': 'END_gatk', \
                                 'Unnamed: 3': 'MAPPABILITY', 'Unnamed: 4': 'SEGDUP', 'Unnamed: 5': 'REPEAT'}\
                        ,inplace=True)
concordant_gatk_annot['CHR_gatk'] = 'chr' + concordant_gatk_annot['CHR_gatk'].astype(str)
concordant_gatk_annot = concordant_gatk_annot[concordant_gatk_annot['AnnotSV type']=='full']

# Import XHMM annotations
concordant_xhmm_annot = pd.read_table(project_dir+'/annotate/concordant_xhmm_annot2.tsv')
concordant_xhmm_annot.rename(columns={'SV chrom': 'CHR_xhmm', 'SV start': 'START_xhmm', 'SV end': 'END_xhmm', \
                                 'Unnamed: 3': 'MAPPABILITY', 'Unnamed: 4': 'SEGDUP', 'Unnamed: 5': 'REPEAT'}\
                        ,inplace=True)
concordant_xhmm_annot['CHR_xhmm'] = 'chr' + concordant_xhmm_annot['CHR_xhmm'].astype(str)
concordant_xhmm_annot = concordant_xhmm_annot[concordant_xhmm_annot['AnnotSV type']=='full']

# Merge CNV annotations
combined = pd.merge(all_combined, concordant_gatk_annot.drop_duplicates(), \
                    on=['CHR_gatk', 'START_gatk', 'END_gatk'], \
                    how='left')
combined = pd.merge(combined, concordant_xhmm_annot.drop_duplicates(), \
                   on=['CHR_xhmm', 'START_xhmm', 'END_xhmm'], \
                   how='left', suffixes=['_gatk', '_xhmm'])
combined = combined.dropna(axis='columns', how='all').T.drop_duplicates().T

combined.to_csv(project_dir+'/output/all_cnv_annot.txt', sep='\t', header=True, index=False)
