#!/bin/bash
############################################################
############################################################
#SBATCH -N 1 
#SBATCH -t 10:00:00 
#SBATCH -J 00_GetDepthOfCoverage
#SBATCH --output=./scoop/logs/00_GetDepthOfCoverage-%A_%a.out
#SBATCH --array=1-985
#SBATCH --cpus-per-task=1
############################################################
############################################################

# Based on "Using XHMM Software to Detect Copy Number
# Variation in Whole-Exome Sequencing Data"
# by Menachem Fromer and Shaun Purcell
# published in Current Protocols in Human Genetics, 2014
# http://atgu.mgh.harvard.edu/xhmm/index.shtml

source $1

outputDir=$PROJECT_DIR/xhmm/SampleReadDepths
mkdir $outputDir

inputBam=$(cat $bam_paths | head -n $SLURM_ARRAY_TASK_ID | tail -n1)

date "+Starting file: $inputBam %Y-%m-%d %H:%M:%S %n"

sampleId=$(basename -s '.rmdup.recal.bam' $inputBam)
outputFile=$outputDir/$sampleId

date "+Getting depth of coverage %Y-%m-%d %H:%M:%S %n"

# Get sequencing depths
java -jar $gatk38/GenomeAnalysisTK.jar \
-T DepthOfCoverage \
-I $inputBam \
-L $intervals \
-R $fasta \
-dt BY_SAMPLE -dcov 5000 -l INFO \
--omitDepthOutputAtEachBase -omitLocusTable \
--minBaseQuality 0 --minMappingQuality 20 \
--start 1 --stop 5000 --nBins 200 \
--includeRefNSites \
--countType COUNT_FRAGMENTS \
-o $outputFile \
&& date "+Finished getting sequence depth %Y-%m-%d %H:%M:%S %n" || exit 1

echo $outputFile'.sample_interval_summary' >> $PROJECT_DIR/xhmm/read_counts.txt

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
############################################################
