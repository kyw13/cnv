#!/bin/bash
############################################################
############################################################
#SBATCH -N 1 
#SBATCH -t 10:00:00
#SBATCH -J 04_GenotypeAll
#SBATCH --output=./scoop/logs/04_GenotypeAll-%A_%a.out
#SBATCH --array=1
#SBATCH --cpus-per-task=1 
#SBATCH -p RM-shared
############################################################
############################################################

# Based on "Using XHMM Software to Detect Copy Number
# Variation in Whole-Exome Sequencing Data"
# by Menachem Fromer and Shaun Purcell
# published in Current Protocols in Human Genetics, 2014
# http://atgu.mgh.harvard.edu/xhmm/index.shtml

# module load xhmm
module load anaconda5/5.0.0-3.6
source activate gatk

PROJECT_DIR='/pylon5/ib5fp9p/kyw13/shared/src/CNV_Detection/xhmm_pipeline/scoop'
READ_DEPTHS=$PROJECT_DIR/SampleReadDepths
TARGET_DIR=$PROJECT_DIR/TargetFilter
filterDir=$PROJECT_DIR/DataFilter
mkdir $filterDir
callsDir=$PROJECT_DIR/CNV_calls
mkdir $callsDir
outputDir=$PROJECT_DIR/CNV_out
mkdir $outputDir

REFERENCE_DIR='/pylon5/ib5fp9p/kyw13/shared/src/resource'
INTERVALS=$REFERENCE_DIR/regions_v4_intersect_v5_union_v3_padded.b37.bed
FASTA=/pylon5/ib5fp9p/wenjuan/GroupPublicInfor/data/b37/human_g1k_v37.fasta

# Joint genotyping
date "+Joint genotyping %Y-%m-%d %H:%M:%S %n"
xhmm --genotype -p xhmm_params.txt \
-r $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores.txt \
-R $filterDir/SampleReadDepths_Filter2.txt \
-g $callsDir/CNV_discover.xcnv \
-F $FASTA \
-v $PROJECT_DIR/ALL_xhmm_CNV_genotypes.vcf \
&& date "+Finished joint genotyping %Y-%m-%d %H:%M:%S %n" || exit 1

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
############################################################
