#!/bin/bash
############################################################
############################################################
#SBATCH -N 1 
#SBATCH -t 10:00:00 
#SBATCH -J 01_CNVOptionalTargetFilter
#SBATCH --output=./logs/01_CNVOptionalTargetFilter-%A_%a.out
#SBATCH --array=1-1
#SBATCH --cpus-per-task=1 

############################################################
############################################################

# Based on "Using XHMM Software to Detect Copy Number
# Variation in Whole-Exome Sequencing Data"
# by Menachem Fromer and Shaun Purcell
# published in Current Protocols in Human Genetics, 2014
# http://atgu.mgh.harvard.edu/xhmm/index.shtml

source $1

targetDir=$PROJECT_DIR/xhmm/TargetFilter
mkdir $targetDir

date "+Calculating GC content of targets %Y-%m-%d %H:%M:%S %n"

# Optional: Calculate GC content of targets
java -jar $gatk38/GenomeAnalysisTK.jar \
-T GCContentByInterval \
-L $intervals \
-R $fasta \
-o $targetDir/locus_GC.txt \
&& date "+Finished calculating GC content of targets %Y-%m-%d %H:%M:%S %n" || exit 1

cat $targetDir_DIR/locus_GC.txt | awk '{if ($2 < 0.1 || $2 > 0.9) print $1}' \
> $targetDir_DIR/extreme_gc_targets.txt \
&& date "+Finished getting extreme GC targets %Y-%m-%d %H:%M:%S %n" || exit 1

# Optional: Calculate sequence complexity of targets
# Load into pseq and calculate repeat masking
date "+Calculating sequence complexity of targets %Y-%m-%d %H:%M:%S %n"

$to_pseq $intervals \
> $targetDir/EXOME.targets.reg \
&& date "+Created PSEQ interval file %Y-%m-%d %H:%M:%S %n" || exit 1

echo -e "#CHR\tPOS1\tPOS2\tID" > $targetDir/EXOME.targets.reg
paste $intervals <(cat -n $intervals | cut -f1 | sed 's/^[ ]*//g') >> $targetDir/EXOME.targets.reg

pseq . loc-load \
--locdb $targetDir/EXOME.targets.LOCDB \
--file $targetDir/EXOME.targets.reg \
--group targets \
--out $targetDir/EXOME.targets.LOCDB.loc-load \
&& date "+Loaded into PSEQ %Y-%m-%d %H:%M:%S %n" || exit 1

pseq . seq-load \
--seqdb $targetDir/seqdb \
--file $fasta \
--name $build \
--description $today'_cnv_calling' \
--format build=$build repeat-mode=N \
&& date "+Loaded Fasta into PSEQ %Y-%m-%d %H:%M:%S %n" || exit 1

pseq . loc-stats \
--locdb $targetDir/EXOME.targets.LOCDB \
--group targets \
--seqdb $targetDir/seqdb | \
awk '{if (NR > 1) print $_}' | sort -k1 -g | awk '{print $10}' | paste $INTERVALS - | \
awk '{print $1"\t"$2}' \
> $targetDir/locus_complexity.txt \
&& date "+Created locus complexity file %Y-%m-%d %H:%M:%S %n" || exit 1

cat $targetDir/locus_complexity.txt | awk '{if ($2 > 0.25) print $1}' \
> $targetDir/low_complexity_targets.txt \
&& date "+Got low complexity targets %Y-%m-%d %H:%M:%S %n" || exit 1

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
############################################################
