#!/bin/bash
############################################################
############################################################
#SBATCH -N 1 
#SBATCH -t 10:00:00 
#SBATCH -J 02_CNVNormalizeAndCall
#SBATCH --output=./logs/02_CNVNormalizeAndCall-%A_%a.out
#SBATCH --array=1
#SBATCH --cpus-per-task=1 

############################################################
############################################################

# Based on "Using XHMM Software to Detect Copy Number
# Variation in Whole-Exome Sequencing Data"
# by Menachem Fromer and Shaun Purcell
# published in Current Protocols in Human Genetics, 2014
# http://atgu.mgh.harvard.edu/xhmm/index.shtml

source $1

READ_DEPTHS=$PROJECT_DIR/xhmm/SampleReadDepths
TARGET_DIR=$PROJECT_DIR/xhmm/TargetFilter
filterDir=$PROJECT_DIR/xhmm/Filter
mkdir $filterDir
callsDir=$PROJECT_DIR/xhmm/CNV_calls
mkdir $callsDir

# Get read depth files
# find $READ_DEPTHS -type f | egrep '.sample_interval_summary$' > $PROJECT_DIR/SampleReadFiles.txt

# Combine depth of coverage outputs
date "+Combining read depth data %Y-%m-%d %H:%M:%S %n"
$xhmm --mergeGATKdepths \
-o $PROJECT_DIR/xhmm/SampleReadDepths.txt \
--GATKdepthsList $PROJECT_DIR/read_counts.txt \
&& date "+Combined read depth data %Y-%m-%d %H:%M:%S %n" || exit 1

# Filter samples and targets and prepare for normalization
# Remove exclude targets if optional target filtering was not performed
date "+Filtering samples and targets %Y-%m-%d %H:%M:%S %n"
$xhmm --matrix \
-r $PROJECT_DIR/xhmm/SampleReadDepths.txt \
--centerData --centerType target \
-o $filterDir/SampleReadDepths_Filtered_Centered.txt \
--outputExcludedTargets $filterDir/SampleReadDepths_Filtered_Centered_TargetFilt.txt \
--outputExcludedSamples $filterDir/SampleReadDepths_Filtered_Centered_SampleFilt.txt \
--minTargetSize 10 --maxTargetSize 10000 \
--minMeanTargetRD 10 --maxMeanTargetRD 500 \
--minMeanSampleRD 25 --maxMeanSampleRD 200 \
--maxSdSampleRD 150 \
&& date "+Finished filtering samples and targets %Y-%m-%d %H:%M:%S %n" || exit 1
# Unused arguments
# --excludeTargets $TARGET_DIR/extreme_gc_targets.txt \
# --excludeTargets $TARGET_DIR/low_complexity_targets.txt \

# Run PCA on mean-centered data
date "+Running PCA %Y-%m-%d %H:%M:%S %n"
$xhmm --PCA \
-r $filterDir/SampleReadDepths_Filtered_Centered.txt \
--PCAfiles $filterDir/SampleReadDepths_Filtered_Centered_PCA \
&& date "+Finished PCA %Y-%m-%d %H:%M:%S %n" || exit 1

# Normalize mean centered data using PCA information
date "+Normalizing mean centered data using PCA %Y-%m-%d %H:%M:%S %n"
$xhmm --normalize \
-r $filterDir/SampleReadDepths_Filtered_Centered.txt \
--PCAfiles $filterDir/SampleReadDepths_Filtered_Centered_PCA \
--normalizeOutput $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized.txt \
--PCnormalizeMethod PVE_mean --PVE_mean_factor 0.7 \
&& date "+Finished normalizing %Y-%m-%d %H:%M:%S %n" || exit 1

# Filter and calculate z-scores for the data
date "+Filtering and calculating z-scores %Y-%m-%d %H:%M:%S %n"
$xhmm --matrix \
-r $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized.txt \
--centerData --centerType sample --zScoreData \
-o $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores.txt \
--outputExcludedTargets $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores_TargetFilt.txt \
--outputExcludedSamples $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores_SampleFilt.txt \
--maxSdTargetRD 30 \
&& date "+Finished filtering and calculating z-scores %Y-%m-%d %H:%M:%S %n" || exit 1

# Restrict original data to filtered and normalized data
date "+Restricting original data %Y-%m-%d %H:%M:%S %n"
$xhmm --matrix \
-r $PROJECT_DIR/xhmm/SampleReadDepths.txt \
--excludeTargets $filterDir/SampleReadDepths_Filtered_Centered_TargetFilt.txt \
--excludeTargets $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores_TargetFilt.txt \
--excludeSamples $filterDir/SampleReadDepths_Filtered_Centered_SampleFilt.txt \
--excludeSamples $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores_SampleFilt.txt \
-o $filterDir/SampleReadDepths_Filter2.txt \
&& date "+Finished restricting original data %Y-%m-%d %H:%M:%S %n" || exit 1

# Params correspond to:
# Exome-wide CNV rate
# Mean number of targets in CNV call
# Mean distance between targets within CNV (in KB)
# Mean of deletion z-score distribution
# SD of deletion z-score distribution
# Mean of idploid z-score distribution
# SD of diploid z-score distribution
# Mean of duplication z-score distribution
# SD of duplication z-score distribution

# Call CNVs in normalized data
date "+Calling CNVs %Y-%m-%d %H:%M:%S %n"
$xhmm --discover -p xhmm_params.txt \
-r $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores.txt \
-R $filterDir/SampleReadDepths_Filter2.txt \
-c $callsDir/CNV_discover.xcnv \
-a $PROJECT_DIR/output/CNV_discover.aux_xcnv \
-s $callsDir/CNV_discover \
&& date "+Finished calling CNVs %Y-%m-%d %H:%M:%S %n" || exit 1

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
############################################################

