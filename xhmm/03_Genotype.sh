#!/bin/bash
############################################################
############################################################
#SBATCH -N 1 
#SBATCH -t 00:30:00 
#SBATCH -J 03_Genotype
#SBATCH --output=./logs/03_Genotype-%A_%a.out
#SBATCH --array=1-985
#SBATCH --cpus-per-task=1 
#SBATCH -p RM-small
############################################################
############################################################

# Based on "Using XHMM Software to Detect Copy Number
# Variation in Whole-Exome Sequencing Data"
# by Menachem Fromer and Shaun Purcell
# published in Current Protocols in Human Genetics, 2014
# http://atgu.mgh.harvard.edu/xhmm/index.shtml

source $1

PROJECT_DIR='/pylon5/ib5fp9p/kyw13/shared/src/CNV_Detection/xhmm_pipeline/scoop'
filterDir=$PROJECT_DIR/xhmm/Filter
callsDir=$PROJECT_DIR/xhmm/CNV_calls
outputDir=$PROJECT_DIR/xhmm/CNV_out
mkdir $outputDir

sampleId=$(basename -a -s '.sample_interval_summary' $(cat $PROJECT_DIR/xhmm/read_counts.txt) | head -n $SLURM_ARRAY_TASK_ID | tail -n1)

# Joint genotyping
date "+Sample genotyping %Y-%m-%d %H:%M:%S %n"
$xhmm --genotype -p xhmm_params.txt \
-r $filterDir/SampleReadDepths_Filtered_Centered_PCA_normalized_SampleZscores.txt \
-R $filterDir/SampleReadDepths_Filter2.txt \
-g $PROJECT_DIR/output/CNV_discover.xcnv \
-F $fasta \
-v $outputDir/$sampleId'_CNV_genotypes.vcf' \
--keepSampleIDs <(echo $sampleId) \
&& date "+Finished sample genotyping %Y-%m-%d %H:%M:%S %n" || exit 1

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
############################################################
