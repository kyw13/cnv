# CNV Calling

CNV calling, filtering, and annotation on exome sequencing data using GATK and XHMM methods

## Run

To run, make sure all files are executable, enter the paths to required input file of all BAM (with duplicates removed and recalibrated) paths, software, fasta file, and intervals bed file in arguments.sh, and execute run.sh. 

This was originally run on a cluster using sbatch, so sample level scripts referencing an array of files may need to be edited to run in parallel in another manner: 
- gatk/00_CollectReadCounts.sh 
- gatk/02_CallCNV.sh 
- gatk/03_PostprocessCNV.sh 
- xhmm/00_GetDepthOfCoverage.sh 
- xhmm/03_Genotype.sh 

## GATK

Copy number variant discovery tool documentation at https://software.broadinstitute.org/gatk/documentation/tooldocs/current/  
Parameters based on https://gatkforums.broadinstitute.org/wdl/discussion/comment/47511  
Workflow from https://github.com/broadinstitute/gatk/blob/master/scripts/cnv_wdl/germline/cnv_germline_cohort_workflow.wdl  

## XHMM

Based on "Using XHMM Software to Detect Copy Number Variation in Whole-Exome Sequencing Data" by Menachem Fromer and Shaun Purcell published in Current Protocols in Human Genetics, 2014
http://atgu.mgh.harvard.edu/xhmm/index.shtml

## Required software

Many are available on conda 
python (with numpy and pandas)  
GATK  
XHMM  
bcftools 
pybedtools  
AnnotSV  
