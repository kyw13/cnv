#!/bin/sh

#  arguments.sh
#  
#
#  Created by Kylia Williams on 3/24/19.
#  

today=$(date +'%Y%m%d')

# Project dir
PROJECT_DIR=./$today

# Input files
bam_paths=
fasta=
intervals=

# Software (alternatively, you can use a conda environment with these packages installed)
# java, python, and pybedtools are also used for postprocessing
gatk=
java=
bcftools=
xhmm=
annotSV=
bedtools=
# GATK 3.8 is required for xhmm
gatk38=

# Scripts
mergecalls=./gatk/mergeCalls.py
to_pseq=./xhmm/interval_list_to_pseq_reg
annotate_concordant=./annotate/annotate_concordant.sh

# Other resources
build='b37'
ploidy_priors=./resources/$build/contig_ploidy_priors_homo_sapiens.tsv
mappability=./resources/$build/k100.umap.formatted.bed
segdup=./resources/$build/segDup.formatted.bed
repeats=./resources/$build/repeatMasker.formatted.bed
