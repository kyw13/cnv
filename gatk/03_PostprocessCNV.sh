#!/bin/sh

############################################################
############################################################
#SBATCH -N 1
#SBATCH -t 10:00:00
#SBATCH -J 03_PostprocessCNV 
#SBATCH --output=./logs/03_PostprocessCNV-%A_%a.out
#SBATCH --array=1-985
#SBATCH --cpus-per-task=1
#SBATCH -p RM-shared
############################################################
############################################################

# Parameters based on https://gatkforums.broadinstitute.org/wdl/discussion/comment/47511
# Workflow from https://github.com/broadinstitute/gatk/blob/master/scripts/cnv_wdl/germline/cnv_germline_cohort_workflow.wdl

source $1

outputDir=$PROJECT_DIR/gatk/CNV_out
mkdir $outputDir

sampleIndex=$(expr $SLURM_ARRAY_TASK_ID - 1)
sampleId=$(basename -s '.counts.hdf5' $(cut -f2 -d ' '  $PROJECT_DIR/gatk/read_counts.txt | head -n $SLURM_ARRAY_TASK_ID | tail -n1))

# Postprocessing of germline CNV calls
date "+Postprocessing germline CNV calls of $sampleId %Y-%m-%d %H:%M:%S %n"
$gatk PostprocessGermlineCNVCalls \
--interval-merging-rule OVERLAPPING_ONLY \
--arguments_file $PROJECT_DIR/gatk/postprocess_args.txt \
--allosomal-contig chr21 \
--allosomal-contig chrX \
--allosomal-contig chrY \
--autosomal-ref-copy-number 2 \
--contig-ploidy-calls $PROJECT_DIR/gatk/CNVContigPloidy-calls \
--sample-index $sampleIndex \
--output-genotyped-intervals $outputDir/$sampleId'_Genotyped_Intervals.vcf.gz' \
--output-genotyped-segments $outputDir/$sampleId'_Genotyped_Segments.vcf.gz' \
&& date "+Finished postprocessing germline CNV calls %Y-%m-%d %H:%M:%S %n" || exit 1
# Unused arguments
# -L $INTERVALS \
# -R $FASTA \

# Tabix output files
$bcftools index -t $outputDir/$sampleId'_Genotyped_Intervals.vcf.gz'
$bcftools index -t $outputDir/$sampleId'_Genotyped_Segments.vcf.gz'

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
