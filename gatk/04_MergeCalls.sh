#!/bin/sh

############################################################
############################################################
#SBATCH -N 1
#SBATCH -t 48:00:00
#SBATCH -J 04_MergeCalls 
#SBATCH --output=./logs/04_MergeCalls-%A_%a.out
#SBATCH --array=1
#SBATCH --cpus-per-task=1
#SBATCH -p RM-shared
############################################################
############################################################

# Parameters based on https://gatkforums.broadinstitute.org/wdl/discussion/comment/47511
# Workflow from https://github.com/broadinstitute/gatk/blob/master/scripts/cnv_wdl/germline/cnv_germline_cohort_workflow.wdl

source $1

GENO_DIR=$PROJECT_DIR/gatk/CNV_out
output=$PROJECT_DIR/output/CNV_discover.txt

# Merge with python script
date "+Merging CNV discovery %Y-%m-%d %H:%M:%S %n"
$mergecalls $GENO_DIR/*_Genotyped_Segments.vcf.gz $output \
&& date "+Finished merging CNV discovery %Y-%m-%d %H:%M:%S %n" || exit 1

# find $PROJECT_DIR/CNV_out -type f | grep -E '_Genotyped_Intervals.vcf.gz$' > interval_vcfs.txt
# cat interval_vcfs.txt | while read file; do $bcftools query -f '[%SAMPLE\t%ID\t%CN\t%CNQ\n]' $file; done > GATK_genotyped_intervals.txt
# && date "+Finished merging CNV intervals %Y-%m-%d %H:%M:%S %n" || exit 1

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
