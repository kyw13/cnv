#!/bin/sh

############################################################
############################################################
#SBATCH -N 1
#SBATCH -t 10:00:00
#SBATCH -J 01_PreprocessCNV 
#SBATCH --output=./logs/01_PreprocessCNV-%A_%a.out
#SBATCH --array=1-1
#SBATCH --cpus-per-task=1
#SBATCH -p RM-shared
############################################################
############################################################

# Parameters based on https://gatkforums.broadinstitute.org/wdl/discussion/comment/47511
# Workflow from https://github.com/broadinstitute/gatk/blob/master/scripts/cnv_wdl/germline/cnv_germline_cohort_workflow.wdl

source $1

intervalDir=$PROJECT_DIR/gatk/SplitIntervals
mkdir $intervalDir

# Annotate intervals with GC content
date "+Annotating intervals %Y-%m-%d %H:%M:%S %n"
$gatk AnnotateIntervals \
-L $intervals \
-R $fasta \
--interval-merging-rule OVERLAPPING_ONLY \
-O $PROJECT_DIR/gatk/annotated_intervals.tsv \
&& date "+Finished annotating intervals %Y-%m-%d %H:%M:%S %n" || exit 1
# Other arguments in gatk 4.1.0.0:
# feature query lookahead
# --mappability-track k100.umap.bed \
# --segmental-duplication-track segDup.bed \
# These last two arguments are only present in gatk 4.0.9.0

# Manually filter based on mappability and counts

# Scatter intervals to process separately
date "+Scattering intervals %Y-%m-%d %H:%M:%S %n"
nGroups=32
split -n l/$nGroups --numeric-suffixes $intervals $intervalDir/intervals.scattered.
for i in $intervalDir/intervals.scattered.*; do mv $i $i.bed; done
find $intervalDir -type f | egrep 'intervals.scattered.+bed$' > $PROJECT_DIR/gatk/scattered_intervals.txt
date "+Finished scattering intervals %Y-%m-%d %H:%M:%S %n"

# Determine germline contig ploidy
date "+Determining germline contig ploidy %Y-%m-%d %H:%M:%S %n"
$gatk DetermineGermlineContigPloidy \
--arguments_file $PROJECT_DIR/gatk/read_counts.txt \
--output $PROJECT_DIR/gatk \
--output-prefix CNVContigPloidy \
--contig-ploidy-priors $ploidy_priors \
--verbosity DEBUG \
--mean-bias-standard-deviation 1.0 \
--mapping-error-rate 0.01 \
--global-psi-scale 0.05 \
--sample-psi-scale 0.001 \
&& date "+Finished determining germline contig ploidy %Y-%m-%d %H:%M:%S %n" || exit 1

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
