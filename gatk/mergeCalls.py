#!/usr/bin/env python

import pandas as pd
import numpy as np
import re
import sys
import os

filenames = sys.argv[1:-1]
output = sys.argv[-1]
all_data = []

for i in filenames:
    basename = os.path.basename(i)
    sample = re.sub('_Genotyped_Segments.vcf.gz$', '', basename)
    data = pd.read_table(i, comment='#', header=None)
    recode = pd.DataFrame([sample]*len(data), columns=['SAMPLE'])
    start = data[2].str.split('_').str[2].astype(int)
    end = data[2].str.split('_').str[3].astype(int)
    chrom = data[0]
    recode['GT'] = data[9].str.split(':').str[0]
    recode['CNV'] = '.'
    recode['INTERVAL'] = data[2].str.split('_').str[1] + ':' + start.astype(str) + '-' + end.astype(str)
    recode['KB'] = (end - start) / 1000.0
    recode['CHR'] = chrom
    recode['MID_BP'] = np.round((end + start) / 2.0).astype(int)
    recode['NUM_TARG'] = data[9].str.split(':').str[2]
    recode['CN'] = data[9].str.split(':').str[1]
    recode['Q_EXACT'] = data[9].str.split(':').str[3]
    recode['Q_SOME'] = data[9].str.split(':').str[4]
    recode['Q_START'] = data[9].str.split(':').str[6]
    recode['Q_STOP'] = data[9].str.split(':').str[5]
    recode = recode[(recode.GT=='1')|(recode.GT=='2')]
    recode['CNV'] = np.where(recode.GT=='1', 'DEL','DUP')
    recode.drop(['GT'], axis=1, inplace=True)
    all_data.append(recode)

final = pd.concat(all_data)
final.to_csv(output, header=True, index=False, sep='\t')

