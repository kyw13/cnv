#!/bin/sh

############################################################
############################################################
#SBATCH -N 1
#SBATCH -t 05:00:00
#SBATCH -J 00_CollectReadCounts
#SBATCH --output=./logs/00_CollectReadCounts-%A_%a.out
#SBATCH --array=1-985
#SBATCH --cpus-per-task=1
#SBATCH -p RM-shared
############################################################
############################################################

source $1

countsDir=$PROJECT_DIR/gatk/SampleReadCounts
mkdir $countsDir

inputBam=$(cat $bam_paths | head -n $SLURM_ARRAY_TASK_ID | tail -n1)

date "+Starting file: $inputBam %Y-%m-%d %H:%M:%S %n"

sampleId=$(basename -s '.rmdup.recal.bam' $inputBam)
outputFile=$countsDir/$sampleId".counts.hdf5"

# Collect counts
date "+Collecting read counts %Y-%m-%d %H:%M:%S %n"
$gatk CollectReadCounts \
-L $intervals \
--input $inputBam \
-R $fasta \
--interval-merging-rule OVERLAPPING_ONLY \
--format HDF5 \
-O $outputFile \
&& date "+Finished collecting read counts  %Y-%m-%d %H:%M:%S %n" || exit 1

echo "--input $outputFile" >> $PROJECT_DIR/read_counts.txt

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
