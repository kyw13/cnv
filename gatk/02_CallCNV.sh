#!/bin/sh

############################################################
############################################################
#SBATCH -N 1
#SBATCH -t 48:00:00
#SBATCH -J 02_CallCNV 
#SBATCH --output=./logs/02_CallCNV-%A_%a.out
#SBATCH --array=1-985
#SBATCH --cpus-per-task=1

############################################################
############################################################

# Parameters based on https://gatkforums.broadinstitute.org/wdl/discussion/comment/47511
# Workflow from https://github.com/broadinstitute/gatk/blob/master/scripts/cnv_wdl/germline/cnv_germline_cohort_workflow.wdl

source $1

callsDir=$PROJECT_DIR/gatk/CNV_calls
mkdir $callsDir
INTERVALS_LIST=$PROJECT_DIR/gatk/scattered_intervals.txt

# Process scattered intervals
inputBed=$(cat $INTERVALS_LIST | head -n $SLURM_ARRAY_TASK_ID | tail -n1)
intervalId=$(basename -s '.bed' $inputBed)

# Run germline CNV caller
date "+Calling germline CNVs %Y-%m-%d %H:%M:%S %n"
$gatk GermlineCNVCaller \
--run-mode COHORT \
-L $inputBed \
--arguments_file $PROJECT_DIR/gatk/read_counts.txt \
--contig-ploidy-calls $PROJECT_DIR/gatk/CNVContigPloidy-calls \
--annotated-intervals $PROJECT_DIR/gatk/annotated_intervals.tsv \
--interval-merging-rule OVERLAPPING_ONLY \
--output $callsDir \
--output-prefix $intervalId \
--verbosity DEBUG \
--p-alt 0.001 \
--p-active 0.01 \
--cnv-coherence-length 100000 \
--class-coherence-length 100000 \
--max-copy-number 5 \
--max-bias-factors 16 \
--mapping-error-rate 0.01 \
--interval-psi-scale 0.002 \
--sample-psi-scale 1e-7 \
--depth-correction-tau 100.0 \
--log-mean-bias-standard-deviation 10.0 \
--init-ard-rel-unexplained-variance 0.1 \
--num-gc-bins 20 \
--gc-curve-standard-deviation 1.0 \
--copy-number-posterior-expectation-mode EXACT \
--enable-bias-factors true \
--active-class-padding-hybrid-mode 50000 \
--learning-rate 0.03 \
--adamax-beta-1 0.9 \
--adamax-beta-2 0.97 \
--log-emission-samples-per-round 50 \
--log-emission-sampling-median-rel-error 0.001 \
--log-emission-sampling-rounds 20 \
--max-advi-iter-first-epoch 5000 \
--max-advi-iter-subsequent-epochs 200 \
--min-training-epochs 10 \
--max-training-epochs 100 \
--initial-temperature 2.0 \
--num-thermal-advi-iters 4000 \
--convergence-snr-averaging-window 500 \
--convergence-snr-trigger-threshold 0.2 \
--convergence-snr-countdown-window 10 \
--max-calling-iters 20 \
--caller-update-convergence-threshold 1e-6 \
--caller-external-admixing-rate 1.0 \
--caller-internal-admixing-rate 0.5 \
--disable-annealing false \
&& date "+Finished calling germline CNVs %Y-%m-%d %H:%M:%S %n" || exit 1

# Append calls and model directories to arguments file
echo "--calls-shard-path $callsDir/$intervalId-calls" >> $PROJECT_DIR/gatk/postprocess_args.txt
echo "--model-shard-path $callsDir/$intervalId-model" >> $PROJECT_DIR/gatk/postprocess_args.txt

date "+Done %Y-%m-%d %H:%M:%S %n"

############################################################
############################################################
